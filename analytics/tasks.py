from celery.decorators import task

from celery.task.schedules import crontab
from celery.decorators import periodic_task
# Register your models here.
from datetime import timedelta
from django.utils import timezone
from .models import RedirectionHistory, URLReports
from django.db.models import Count, Sum
from shortener.models import UserURL
from django.db.models import F

@periodic_task(run_every=(crontab(minute=0,hour=0)),name="daily_update",ignore_result=True)
def run_daily_update():
    now = timezone.now()
    URLReports.objects.all().update(yesterday_count=F('daily_count'))
    URLReports.objects.all().update(daily_count=0)
    URLReports.objects.all().update(weekly_count=0)
    URLReports.objects.all().update(monthly_count=0)
    
    new_reports = RedirectionHistory.objects.filter(created__gte=now-timedelta(days=7)).values(
    'url', 'session_id', 'browser', 'is_mobile').annotate(new_count=Count('created'))
    for r in new_reports:
        report = URLReports.objects.filter(
            url=r['url'], session_id=r['session_id'], browser=r['browser'], is_mobile=r['is_mobile']).first()
        if(report):
            report.weekly_count = r['new_count']
            report.save()
        else:
            new = URLReports(weekly_count=r['new_count'], url=UserURL.objects.get(pk=r['url']) ,
                                session_id=r['session_id'], browser=r['browser'], is_mobile=r['is_mobile'])
            new.save()
    
    new_reports = RedirectionHistory.objects.filter(created__gte=now-timedelta(days=30)).values(
    'url', 'session_id', 'browser', 'is_mobile').annotate(new_count=Count('created'))
    for r in new_reports:
        report = URLReports.objects.filter(
            url=r['url'], session_id=r['session_id'], browser=r['browser'], is_mobile=r['is_mobile']).first()
        if(report):
            report.monthly_count = r['new_count']
            report.save()
        else:
            new = URLReports(monthly_count=r['new_count'], url=UserURL.objects.get(pk=r['url']) ,
                                session_id=r['session_id'], browser=r['browser'], is_mobile=r['is_mobile'])
            new.save()
    URLReports.objects.filter(monthly_count=0,yesterday_count=0,weekly_count=0).delete()


@periodic_task(run_every=(crontab(minute='*/2')), name="update", ignore_result=True)
def update():
    now = timezone.now()
    new_reports = RedirectionHistory.objects.filter(created__gte=now-timedelta(minutes=2),seen=False).values(
    'url', 'session_id', 'browser', 'is_mobile').annotate(new_count=Count('created'))
    for r in new_reports:
        report = URLReports.objects.filter(
            url=r['url'], session_id=r['session_id'], browser=r['browser'], is_mobile=r['is_mobile']).first()
        if(report):
            report.daily_count += r['new_count']
            report.save()
            
        else:
            new = URLReports(daily_count=r['new_count'], url=UserURL.objects.get(pk=r['url']) ,
                                session_id=r['session_id'], browser=r['browser'], is_mobile=r['is_mobile'])
            new.save()
    new_reports.update(seen=True)
 