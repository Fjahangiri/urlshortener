from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from django.core.validators import URLValidator
from django.core.exceptions import ValidationError
from django.conf import settings
from shortener.models import UserURL
from analytics.models import URLReports
from django.db.models import Count, Sum
from .serializers import *
from .tasks import update

class AnalyticsView(APIView):
    def get_query_result(self, duration, urls):
        if(duration == "today"):
            return URLReports.objects.filter(url__in=urls, daily_count__gt=0)
        if(duration == "yesterday"):
            return URLReports.objects.filter(url__in=urls, yesterday_count__gt=0)
        if(duration == "week"):
            return URLReports.objects.filter(url__in=urls, weekly_count__gt=0)
        if(duration == "month"):
            return URLReports.objects.filter(url__in=urls, monthly_count__gt=0)

    def get_count_name(self, duration):
        if(duration == "today"):
            return "daily_count"
        if(duration == "yesterday"):
            return "yesterday_count"
        if(duration == "week"):
            return "weekly_count"
        if(duration == "month"):
            return "monthly_count"
        else:
            return "error"

    def serialize_response(self, report_list,duration):
        all_urls = report_list.values('url').annotate(
            count=Sum(self.get_count_name(duration)))
        browser_urls = report_list.values('url',
                                          'browser').annotate(count=Sum(self.get_count_name(duration)))
        mobile_urls = report_list.values('url',
                                         'is_mobile').annotate(count=Sum(self.get_count_name(duration)))
        all_session = report_list.values('url', 'session_id').annotate(
            count=Sum(self.get_count_name(duration)))
        all_session_device = report_list.values('session_id', 'url',
                                                'is_mobile').annotate(count=Sum(self.get_count_name(duration)))
        all_session_browser = report_list.values('session_id', 'url',
                                                 'browser').annotate(count=Sum(self.get_count_name(duration)))
        data1 = {}
        data1['all_urls'] = AllUrlsSerializer(all_urls, many=True).data
        data1['mobile_urls'] = IsMobileSerializer(
            mobile_urls, many=True).data
        data1['browser_urls'] = BrowserSerializer(
            browser_urls, many=True).data

        data2 = {}
        data2['all_urls'] = AllUrlsSerializer(all_session, many=True).data
        data2['mobile_urls'] = IsMobileSerializer(
            all_session_device, many=True).data
        data2['browser_urls'] = BrowserSerializer(
            all_session_browser, many=True).data

        final = {}
        final['users'] = data2
        final['clicks'] = data1
        return final

    def get(self, request, duration):
        
        if(request.user and not self.get_count_name(duration)=="error"):
            update()
            urls = UserURL.objects.filter(
                user=request.user).values_list('short_url')
            report_list = self.get_query_result(duration, urls)
            return Response(AnalyticsSerializer(self.serialize_response(report_list,duration)).data, status.HTTP_200_OK)
        return Response({"error": "bad reqeuest"},status.HTTP_400_BAD_REQUEST)
