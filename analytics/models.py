from django.db import models
from datetime import datetime, timedelta
from django.db.models import Count

class RedirectionHistory(models.Model):
    url = models.ForeignKey(
        'shortener.UserURL', on_delete=models.CASCADE, related_name="url_history")
    browser = models.CharField(max_length=30)
    is_mobile = models.BooleanField()
    session_id = models.CharField(max_length=35)
    created = models.DateTimeField(default=datetime.now)
    seen = models.BooleanField(default=False)

    class Meta:
        indexes = [
            models.Index(fields=['url','is_mobile','session_id','browser']),
        ]

    def __str__(self):
        return str(self.url)+": "+self.session_id

class URLReports(models.Model):
    url = models.ForeignKey(
        'shortener.UserURL', on_delete=models.CASCADE, related_name="url_report")
    browser = models.CharField(max_length=30)
    is_mobile = models.BooleanField()
    session_id = models.CharField(max_length=35)
    daily_count=models.IntegerField(default=0)
    yesterday_count=models.IntegerField(default=0)
    weekly_count=models.IntegerField(default=0)
    monthly_count=models.IntegerField(default=0)
    class Meta:
        unique_together= ('url','browser','is_mobile','session_id')
        indexes= [
            models.Index(fields=['url'])
        ]


    