from rest_framework import serializers
from authentication.models import User


class IsMobileSerializer(serializers.Serializer):
    is_mobile = serializers.BooleanField()
    count = serializers.IntegerField()
    url = serializers.URLField()


class BrowserSerializer(serializers.Serializer):
    browser = serializers.CharField(max_length=30)
    count = serializers.IntegerField()
    url = serializers.URLField()


class AllUrlsSerializer(serializers.Serializer):
    count = serializers.IntegerField()
    url = serializers.URLField()


class CountSerializer(serializers.Serializer):
    all_urls = AllUrlsSerializer(many=True)
    mobile_urls = IsMobileSerializer(many=True)
    browser_urls = BrowserSerializer(many=True)


class AnalyticsSerializer(serializers.Serializer):
    users = CountSerializer()
    clicks = CountSerializer()
