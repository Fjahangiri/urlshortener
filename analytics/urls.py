
from django.urls import path
from .views import AnalyticsView
urlpatterns = [
    path('<str:duration>', AnalyticsView.as_view(), name='token_refresh'),
    
]
