from django.urls import path
from .views import URLCreate
urlpatterns = [
    path('url/', URLCreate.as_view(), name='url_create')
]
