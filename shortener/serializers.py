from rest_framework import serializers
from .models import UserURL
from authentication.models import User


class UserURLSerializer(serializers.ModelSerializer):

    class Meta:
        model = UserURL
        fields = ('full_url', 'short_url')
