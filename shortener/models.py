from django.db import models

# Create your views here.


class UserURL(models.Model):
    full_url = models.URLField(max_length=2000)
    short_url = models.URLField(max_length=200, primary_key=True)
    count = models.PositiveSmallIntegerField(default=1)
    user = models.ForeignKey(
        'authentication.User', on_delete=models.CASCADE, related_name="user_urls")


    def __str__(self):
        return self.short_url
