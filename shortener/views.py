from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from .serializers import UserURLSerializer
from authentication.models import User
from .models import UserURL
import time
from django.core.validators import URLValidator
from django.core.exceptions import ValidationError
from django.conf import settings
import redis


class URLCreate(APIView):
    serializer_class = UserURLSerializer
    url_validate = URLValidator()
    redis_instance = redis.StrictRedis(host=settings.REDIS_HOST,
                                       port=settings.REDIS_PORT, db=0)

    def post(self, request, format='json'):
        if(request.data.get('full_url', None)):
            try:
                self.url_validate(request.data.get('full_url'))
            except ValidationError:
                return Response({"error": "invalid url"}, status=status.HTTP_400_BAD_REQUEST)

            path = request.data.get('path', "")
            url_path = settings.REDIRECT_PATH+path
            if(len(path) < 1):
                ts = time.time()
                url_path += str(hash(request.data['full_url']+str(ts)))
            else:
                existed_url = UserURL.objects.filter(
                    short_url=url_path).first()
                if(existed_url):
                    url_path += "_"+str(existed_url.count)
                    existed_url.count += 1
                    existed_url.save()
            new_url = UserURL(user=request.user, full_url=request.data['full_url'], short_url=url_path)
            serializer = self.serializer_class(new_url)
            new_url.save()
            self.redis_instance.set(new_url.short_url, new_url.full_url)
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response({"error": "url field is required"}, status=status.HTTP_400_BAD_REQUEST)
