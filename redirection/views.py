from rest_framework.views import APIView
from django.db import models
from django.conf import settings
from rest_framework import status, permissions
from rest_framework.response import Response
from django.shortcuts import redirect
import redis
from .tasks import add_analytics
#from analytics.models import UserSession,RedirectionModel
from datetime import timedelta
from django.db.models import Count,Sum
from django.utils import timezone
from analytics.models import RedirectionHistory,URLReports
from shortener.models import UserURL

class RedirectView(APIView):
    permission_classes = (permissions.AllowAny,)

    redis_instance = redis.StrictRedis(host=settings.REDIS_HOST,
                                       port=settings.REDIS_PORT, db=0)

    def get(self, request, resource):
        print(request.session.session_key)
        if not request.session.session_key:
            request.session.create()
        session_id = request.session.session_key
        full_url = self.redis_instance.get(settings.REDIRECT_PATH+resource)
        
        if(full_url):
            add_analytics.delay(request.session.session_key,
                                request.user_agent.is_mobile,
                                request.user_agent.browser.family,
                                settings.REDIRECT_PATH+resource)
            return redirect(full_url.decode('utf-8'))
        return Response({"error": "url not existed"}, status=status.HTTP_404_NOT_FOUND)
