from celery.decorators import task
from analytics.models import RedirectionHistory
from shortener.models import UserURL


@task(name="add_analytics")
def add_analytics(session_id, is_mobile, browser, url):
    # session, created = UserSession.objects.get_or_create(
    #     session_id=session_id,
    #     is_mobile=is_mobile,
    #     browser=browser
    # )
    clicked_url = UserURL.objects.get(pk=url)
    new_redirect = RedirectionHistory(
        url=clicked_url, session_id=session_id, is_mobile=is_mobile, browser=browser)
    new_redirect.save()
