from django.urls import path
from .views import RedirectView

urlpatterns = [
    path('<path:resource>', RedirectView.as_view())
]
