from django.urls import path
from rest_framework_simplejwt import views as jwt_views
from .views import UserCreate
from .serializers import CustomJWTSerializer

urlpatterns = [
    path('token/obtain/', jwt_views.TokenObtainPairView.as_view(
        serializer_class=CustomJWTSerializer), name='token_create'),
    path('token/refresh/', jwt_views.TokenRefreshView.as_view(), name='token_refresh'),
    path('create/', UserCreate.as_view(), name="create_user"),
]
